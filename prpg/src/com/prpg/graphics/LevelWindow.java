package com.prpg.graphics;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.peer.KeyboardFocusManagerPeer;
import java.io.File;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import com.prpg.logic.Level;

public class LevelWindow extends JPanel implements KeyListener{
	private static final long serialVersionUID = 1L;
	
	private static ArrayList<Level> levels = new ArrayList<Level>();
	private static int currLevelIndex;
	
	private static LevelWindow w;
	private static JFrame f;
	
	public LevelWindow(){
		super(true);
	}
	
	public static void startPlaying(){
		
		createLevel("Level 1", "res" + File.separatorChar + "res" + File.separatorChar + "character.png",
				"res" + File.separatorChar + "res" + File.separatorChar + "level 1.png",
				"res" + File.separatorChar + "res" + File.separatorChar + "level 1-transparent.png");
		
		
		currLevelIndex = 0;
		
		
		f = new JFrame();
		f.setEnabled(true);
		f.setLayout(new BorderLayout());
		f.requestFocus();
		f.setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/2 - 960/2, Toolkit.getDefaultToolkit().getScreenSize().height/2 - 640/2);
		w = new LevelWindow();
		w.setPreferredSize(new Dimension(960,640));
		w.setLocation(0, 0);
		w.addKeyListener(w);
		w.setVisible(true);
		w.setEnabled(true);
		f.getContentPane().add(w);
		f.setTitle("A great game by Chris Colahan!");
		f.setPreferredSize(new Dimension(960,640));
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		f.setResizable(false);
		f.pack();
		f.setVisible(true);
		
		
		w.repaint();
		
	}
	
	private static void createLevel(String name, String characterPicLoc, String levelPic, String levelTransparentPic){
		levels.add(new Level(name, characterPicLoc, levelPic, levelTransparentPic));
	}
	
	private static void loadNextLevel(){
		if(currLevelIndex != levels.size() - 1 && levels.size() != 0){
			currLevelIndex ++;
		}
	}
	
	private static void loadPreviousLevel(){
		if(currLevelIndex != 0 && levels.size() != 0){
			currLevelIndex --;
		}
	}
	
	protected void paintComponent(Graphics tmpG){
		super.paintComponent(tmpG);
		Graphics2D g = (Graphics2D) tmpG;
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, 960, 640);
		if(levels.get(currLevelIndex).hasWon()){
			loadNextLevel();
		}
		
		levels.get(currLevelIndex).update(g);
		
		repaint();
	}

	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		Level currLevel = levels.get(currLevelIndex);
		switch (e.getKeyCode()) {
		case KeyEvent.VK_UP:
			currLevel.setPlayerLocation(new Point(currLevel.getPlayerLocation().x, currLevel.getPlayerLocation().y - 1));
			break;
		case KeyEvent.VK_DOWN:
			currLevel.setPlayerLocation(new Point(currLevel.getPlayerLocation().x, currLevel.getPlayerLocation().y + 1));
			break;
		case KeyEvent.VK_RIGHT:
			currLevel.setPlayerLocation(new Point(currLevel.getPlayerLocation().x - 1, currLevel.getPlayerLocation().y));
			break;
		case KeyEvent.VK_LEFT:
			currLevel.setPlayerLocation(new Point(currLevel.getPlayerLocation().x + 1, currLevel.getPlayerLocation().y));
			break;
		default:
			break;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
}
