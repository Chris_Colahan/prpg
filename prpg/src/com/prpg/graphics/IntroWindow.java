package com.prpg.graphics;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Toolkit;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JPanel;
import javax.imageio.ImageIO;
import javax.swing.JFrame;

import com.prpg.core.Core;

public class IntroWindow {
	
	//graphics stuff
	private static Image background;
	private static Image start_normal;
	private static Image start_moused_over;
	private static Image quit_normal;
	private static Image quit_moused_over;
	
	private static IntroFrame window;
	
	public IntroWindow(){
		//load intro window images
		try{
			String pathToFolder = null;
			pathToFolder = "res" + File.separatorChar + "res" + File.separatorChar;
			
			
			background = ImageIO.read(new File(pathToFolder + "main menu.png"));
			start_normal = ImageIO.read(new File(pathToFolder + "play button-normal.png"));
			start_moused_over = ImageIO.read(new File(pathToFolder + "play button-hover over.png"));
			quit_normal = ImageIO.read(new File(pathToFolder + "exit button-normal.png"));
			quit_moused_over = ImageIO.read(new File(pathToFolder + "exit button-hover over.png"));
			
			
		}catch(IOException e){
			System.err.print("Missing Images.");
			e.printStackTrace();
			System.exit(0);
		}
		
		//set up the window
		window = new IntroFrame();
		window.setUpFrame();
	}
	
	private static void close(){
		window.close();
	}
	
	public static void open(){
		try {
			IntroWindow.class.newInstance();
		} catch (InstantiationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private class IntroFrame extends JPanel{
		private static final long serialVersionUID = 1L;
		private final Dimension size = new Dimension(960,640);
		private JFrame frame;
		
		public IntroFrame(){
			
		}
		
		
		public void setUpFrame(){
			frame = new JFrame();
			
			//set the size
			
			frame.setLayout(null);
			
			frame.setPreferredSize(size);
			
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			
			//center the frame in the screen
			frame.setLocation(screenSize.width/2 - size.width/2, screenSize.height/2 - size.height/2);
			
			//set the title
			frame.setTitle(Core.TITLE);
			
			//add stuff
			
			PlayButton p = new PlayButton();
			frame.getContentPane().add(p);
			p.setBounds(50, 400, start_normal.getWidth(null) * 10, start_normal.getHeight(null) * 10);
			p.setVisible(true);
			p.setEnabled(true);
			
			ExitButton e = new ExitButton();
			frame.getContentPane().add(e);
			e.setBounds(600, 440, quit_normal.getWidth(null) * 10, quit_normal.getHeight(null) * 10);
			e.setVisible(true);
			e.setEnabled(true);
			
			IntroFrame panel = new IntroFrame();
			panel.setSize(size);
			panel.setLocation(0, 0);
			frame.getContentPane().add(panel);
			
			
			
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.setResizable(false);
			frame.pack();
			frame.setVisible(true);
			try{
			}
			catch(Exception e1){
				e1.printStackTrace();
			}
			
		}
		
		public void close(){
			frame.dispose();
		}
		
		public void paintComponent(Graphics tmpG){
			
			Graphics2D g = (Graphics2D) tmpG;
			g.setColor(Color.BLACK);
			g.fillRect(0, 0, size.width, size.height);
			g.drawImage(background, 0,0, size.width, size.height, null);
			g.dispose();
			super.paintComponent(g);
		}
		
	}
	
	
	private class PlayButton extends JButton implements MouseListener{
		private static final long serialVersionUID = 1L;

		public PlayButton(){
			addMouseListener(this);
			setBorder(null);
			this.setIcon(new ImageIcon(start_normal.getScaledInstance(
					start_normal.getWidth(null) * 10, start_normal.getHeight(null) * 10, 0)));
		}

		@Override
		public void mouseClicked(MouseEvent e) {
			IntroWindow.close();
			LevelWindow.startPlaying();
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			this.setIcon(new ImageIcon(start_moused_over.getScaledInstance(
					start_moused_over.getWidth(null) * 10, start_moused_over.getHeight(null) * 10, 0)));
		}

		@Override
		public void mouseExited(MouseEvent e) {
			this.setIcon(new ImageIcon(start_normal.getScaledInstance(
					start_normal.getWidth(null) * 10, start_normal.getHeight(null) * 10, 0)));
		}

		@Override
		public void mousePressed(MouseEvent e) {
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			
		}
	}
	
	private class ExitButton extends JButton implements MouseListener{
		private static final long serialVersionUID = 1L;

		public ExitButton(){
			setBorder(null);
			this.addMouseListener(this);
			this.setIcon(new ImageIcon(quit_normal.getScaledInstance(
					quit_normal.getWidth(null) * 10, quit_normal.getHeight(null) * 10, 0)));
		}
		
		@Override
		public void mouseClicked(MouseEvent e) {
			System.exit(0);
		}

		@Override
		public void mouseEntered(MouseEvent e) {
			this.setIcon(new ImageIcon(quit_moused_over.getScaledInstance(
					quit_moused_over.getWidth(null) * 10, quit_moused_over.getHeight(null) * 10, 0)));
		}

		@Override
		public void mouseExited(MouseEvent e) {
			this.setIcon(new ImageIcon(quit_normal.getScaledInstance(
					quit_normal.getWidth(null) * 10, quit_normal.getHeight(null) * 10, 0)));
		}

		@Override
		public void mousePressed(MouseEvent e) {
			
		}

		@Override
		public void mouseReleased(MouseEvent e) {
			
		}
		
	}
}