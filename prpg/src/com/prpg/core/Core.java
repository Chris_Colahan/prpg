package com.prpg.core;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;

import com.prpg.graphics.IntroWindow;

public class Core {
	
	public static final boolean debug_mode = true;
	
	public static final String TITLE = "Platform-RPG game by Christopher and Brennan Colahan (v2)";
	
	public static void main(String args[]){
		
		//set up error stuff depending on debug or not
		if(!debug_mode){
			File f = new File("err_log.txt");
			try {
				if(!f.exists()){
					f.createNewFile();
				}
				System.setErr(new PrintStream(f));
					
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				System.exit(0);
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			}
		}
		
		//open the start window
		IntroWindow.open();
	}
}