package com.prpg.logic;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.File;

import javax.imageio.ImageIO;

public class Level {
	
	private String levelName;
	private Image character;
	private Image level;
	private BufferedImage levelTransparent;
	private boolean win = false;
	private Point player;
	
	public Level(String name, String characterPicLoc, String levelPic, String levelTransparentPic){
		player = new Point(0,0);
		levelName = name;
		try{
			character = ImageIO.read(new File(characterPicLoc));
			level = ImageIO.read(new File(levelPic));
			levelTransparent = ImageIO.read(new File(levelTransparentPic));
			
			//get the start and end coords
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public void update(Graphics2D g){
		logic();
		paint(g);
	}
	
	private void logic(){
		
	}
	
	private void paint(Graphics2D g){
		g.setColor(Color.WHITE);
		g.drawImage(level, 0, 0, 960, 640, null);
		g.drawImage(character, player.x, player.y, character.getWidth(null) * 10, character.getHeight(null) * 10, null);
	}
	
	public boolean hasWon(){
		return win;
	}
	
	public void setPlayerLocation(Point p){
		player = p;
	}
	
	public Point getPlayerLocation(){
		return player;
	}
}
